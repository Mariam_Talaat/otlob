<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/view_restaurant', 'API\RestaurantController@index');

// Route::post('/edit_restauraunt', 'RestaurantController@Edit');
// Route::post('/update_restaurant', 'RestaurantController@Update');
// Route::post('/delete_restaurant', 'RestaurantController@Delete');