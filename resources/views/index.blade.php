
@extends('layouts.app')

@section('content')

 <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                      ALL Restaurants
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                    <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                    <thead>

                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Edit</th>
                       
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($restaurants as $restaurant)
                    <tr class="gradeX">
                        <td>{{$restaurant->name}}</td>
                        <td>{{$restaurant->Address}}</td>

                       
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    </div>
                    </div>
                </section>
            </div>
        </div>
        

                 
        <!-- page end-->
        </section>
    </section>

 @endsection   
   