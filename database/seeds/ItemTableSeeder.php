<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $i=0;
        while($i!=3){
         DB::table('items')->insert([
            'item_name' => str_random(10),
            'item_price' => rand(1,1000).rand(1,10),
            'restaurant_id' => '6',
        ]);

         $i++;

     }
    }
}
