<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       $i=0;
        while($i!=3){
         DB::table('restaurants')->insert([
            'name' => str_random(10),
            'address' => str_random(10),
            'phone_number' => '01'.rand(0, 2). rand(1000, 9999) . rand(1000, 9999),
        ]);

          $i++;

     }
    }
}
