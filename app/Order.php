<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Support\Facades\Auth;


class Order extends Model
{
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array

     */
    protected $table= 'orders';

    protected $fillable = [
        'cart_id', 'user_id', 'delivery_area',
    ];


    public function getAll()
    {
        return static::all();
    }
    
    public function findOrder($id)
    {
        return static::find($id);
    }


    public function deleteOrder($id)
    {
        return static::find($id)->delete();
    }



   
}