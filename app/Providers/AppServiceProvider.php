<?php

namespace App\Providers;

use App\Repositories\ItemRepository;
use App\Repositories\RestaurantRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\RestaurantRepository');
        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\OrderRepository');
        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\ItemRepository');
        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\CartIemRepository');
    }
}
