<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Support\Facades\Auth;


class Item extends Model
{
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array

     */
    protected $table= 'items';

    protected $fillable = [
        'item_name', 'item_price', 'item_photo','restaurant_id'
    ];


    public function getAll()         //return menus
    {
        return static::all();
    }


    
    public function findItem($id)
    {
        return static::find($id);
    }


    public function deleteItem($id)
    {
        return static::find($id)->delete();
    }

    public function getMenu($restaurant_id)        
    {
        return static::where('restaurant_id',$restaurant_id)->get();
    }

   
}