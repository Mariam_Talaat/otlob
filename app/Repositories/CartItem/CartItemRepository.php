<?php
namespace App\Repositories\CartItem;



use App\CartItem;



class CartRepository 
{
    public $cartItem;


    function __construct(CartItem $cartItem) 
    {

       $this->cartItem = $cartItem;
    }


    public function getAll()
    {
        return $this->cartItem->getAll();
    }

    public function create($id)
    {
        return $this->cartItem->create($id);
    }


    public function find($id)
    {
        return $this->cartItem->findCartItem($id);
    }


    public function delete($id)
    {
        return $this->Cart->deleteCartItem($id);
    }


  }  
