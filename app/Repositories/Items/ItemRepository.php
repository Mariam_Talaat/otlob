<?php
namespace App\Repositories\Items;



use App\Item;



class ItemRepository 
{
    public $item;


    function __construct(Item $item) 
    {

       $this->item = $item;
    }


    public function getAll()
    {
        return $this->item->getAll();
    }


    public function find($id)
    {
        return $this->item->findItem($id);
    }


    public function delete($id)
    {
        return $this->item->deleteItem($id);
    }

    public function getMenu($restaurant_id)        
    {
        return $this->item->getMenu($restaurant_id);
    }


  }  