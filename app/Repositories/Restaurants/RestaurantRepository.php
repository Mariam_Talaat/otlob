<?php  
namespace App\Repositories\Restaurants;



use App\Restaurant;



class RestaurantRepository 
{
    public $restaurant;


    function __construct(Restaurant $restaurant) 
    {

       $this->restaurant = $restaurant;
    }


    public function getAll()
    {
        return $this->restaurant->getAll();
    }


    public function find($id)
    {
        return $this->restaurant->findRestaurant($id);
    }


    public function delete($id)
    {
        return $this->restaurant->deleteRestaurant($id);
    }


  }  
