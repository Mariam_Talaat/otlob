<?php
namespace App\Repositories\Orders;



use App\Order;



class OrderRepository 
{
    public $order;


    function __construct(Order $order) 
    {

       $this->order = $order;
    }


    public function getAll()
    {
        return $this->order->getAll();
    }


    public function find($id)
    {
        return $this->order->findOrder($id);
    }


    public function delete($id)
    {
        return $this->order->deleteOrder($id);
    }


  }  