<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use App\Repositories\RestaurantRepository;

use Illuminate\Support\ServiceProvider;


class RepoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\RepositoryInterface', RestaurantRepository::class);
        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\OrderRepository');
        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\ItemRepository');
        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\CartIemRepository');
    }
}