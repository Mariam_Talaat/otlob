<?php
namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Support\Facades\Auth;


class Restaurant extends Model
{
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array

     */
    protected $table= 'restaurants';

    protected $fillable = [
        'name', 'address', 'phone_number',
    ];


    public function getAll()
    {
        return static::all();
    }
    
    public function findRestaurant($id)
    {
        return static::find($id);
    }


    public function deleteRestaurant($id)
    {
        return static::find($id)->delete();
    }



   
}