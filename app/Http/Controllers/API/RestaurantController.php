<?php 
namespace App\Http\Controllers\API;


//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Restaurant;
use App\Repositories\Restaurants\RestaurantRepository; 
use Illuminate\Support\Facades\Auth;
use Validator;
use Request;

class RestaurantController extends Controller
{


    protected $restaurants;


   	public function __construct(RestaurantRepository $restaurants)
    {
        $this->restaurants = $restaurants;
    }


    public function index(Request $request)
    {    
        
    	$restaurants = $this->restaurants->getAll();
         if(Request::json()) {
            return view('index',compact('restaurants'));
          }  
       

       
   return response()->json(['success'=>$restaurants],200);
    

       
    }

}

