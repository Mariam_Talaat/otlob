<?php

namespace App\Http\Controllers\API;

use App\Item;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\User;
//use App\Restaurant;

//use App\Repositories\RestaurantRepository;
use App\Repositories\ItemRepository;
use Illuminate\Support\Facades\Auth;
use Validator;


class MenuController extends Controller
{


    protected $items;


   	public function __construct(ItemRepository $items)
    {
        $this->items = $items;
    }

    public function index()    //returns all restaurants menus
    {
        $items = $this->items->getAll();
        $menus = $items->groupBy('restaurant_id');
        return response()->json(['success'=>$menus],200);
        
    }

    public function show($id)    //returns certain restaurant menu
    {  

        $menu = $this->items->getMenu($id);
       
        return response()->json(['success'=>$menu],200);
        
    }

     public function delete($id)    //returns certain restaurant menu
    {  

        $menu = $this->items->deleteMenu($id);
       
        return response()->json(['success'=>$menu],200);
        
    }

}

