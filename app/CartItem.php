<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Support\Facades\Auth;


class CartItem extends Model
{
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array

     */
    protected $table= 'carts_items';

    protected $fillable = [
        'cart_id', 'item_id', 'quantity',
    ];


    public function getAll()
    {
        return static::all();
    }
     public function create($id)
    {
        return static::create(['user_id'=>$id]);
    }
    
    public function findCartItem($id)
    {
        return static::find($id);
    }


    public function deleteCartItem($id)
    {
        return static::find($id)->delete();
    }